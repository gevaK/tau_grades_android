package com.geek.tautracker;

import android.util.Pair;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.FormElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by GevaK on 28/10/2016.
 * A very basic class for maintaining sessions with Jsoup - remembering cookies and headers.
 * Can definitely be improved, I only implemented the minimum functionality that I needed.
 */
public class JsoupSession {

    Map<String, String> mCookies;
    ArrayList<Pair<String, String>> mHeaders;
    int mTimeout;
    public JsoupSession() {
        this.mCookies = new HashMap<>();
        this.mHeaders = new ArrayList<>();
        this.mTimeout = 5000;
    }

    public JsoupSession cookies(Map<String, String> cookies) {
        this.mCookies = cookies;
        return this;
    }

    public Connection setConnectionParams(Connection conn) {
        for (Pair<String, String> p : this.mHeaders) {
            conn = conn.header(p.first, p.second);
        }
        conn = conn.cookies(this.mCookies);
        conn = conn.timeout(this.mTimeout);
        return conn;
    }

    public Connection submitForm(FormElement f) {
        Connection conn = f.submit();
        return setConnectionParams(conn);
    }

    public Connection connect(String url) {
        Connection conn = Jsoup.connect(url);
        return setConnectionParams(conn);
    }

    public JsoupSession header(String key, String value) {
        this.mHeaders.add(new Pair<String, String>(key, value));
        return this;
    }

    public JsoupSession timeout(int newTimeout) {
        this.mTimeout = newTimeout;
        return this;
    }

}
