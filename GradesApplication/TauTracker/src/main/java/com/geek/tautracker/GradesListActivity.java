package com.geek.tautracker;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.Map;

public class GradesListActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        // Define a custom screen layout
        setContentView(R.layout.activity_grades_list);

        // Get grades map
        Map<String, ?> gradesMap = getSharedPreferences(QueryService.PREFS_GRADES_MAP_FILE, Context.MODE_PRIVATE).getAll();
        String[] grades = new String[gradesMap.size()];
        //TODO: Sort this by date / name
        //TODO: Create two-column view
        int i = 0;
        for (String key : gradesMap.keySet()) {
            grades[i] = key + " - " + gradesMap.get(key);
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                grades);

        // Bind to our new adapter.
        setListAdapter(adapter);
    }
}
