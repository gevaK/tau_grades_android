
/***
 Copyright (c) 2009-11 CommonsWare, LLC

 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License. You may obtain
 a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package com.geek.tautracker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.commonsware.cwac.wakeful.AlarmReceiver;
import com.commonsware.cwac.wakeful.WakefulIntentService;

import java.util.HashMap;


public class QueryService extends WakefulIntentService {

    final static String NBSP_STRING = "\u00a0"; // In TAU website, no grade is indicated by '&nbsp;'
    final static String PREFS_SERVICE_FILE = "TAU_SERVICE_FILE";
    final static String PREFS_SERVICE_NOTIF_ID = "TAU_NOTIF_ID";
    final static String PREFS_SERVICE_FIRST_RUN= "TAU_FIRST_RUN";
    final static String PREFS_GRADES_MAP_FILE = "TAU_GRADES_FILE";
    final static String PREFS_SCANS_MAP_FILE = "TAU_SCANS_FILE";
    final static String PREFS_WAKEFUL_ALARM_ACTIVE = "IS_ALARM_ACTIVE";
    final static String NOTIFICATION_CHANNEL = "notificationChannel";
    final static long[] VIBRATE_PATTERN = new long[] { 1000, 1000, 1000, 1000};

    public QueryService() {

        super("AppService");
    }

    public static void cancelAlarms(Context ctxt) {
        // Unset the ALARM_ACTIVE flag
        Log.i("TauTracker" + "QueryService", "ALARMS_ACTIVE_FLAG = false");
        SharedPreferences prefs = ctxt.getSharedPreferences("com.commonsware.cwac.wakeful.WakefulIntentService", 0);
        prefs.edit().putBoolean(QueryService.PREFS_WAKEFUL_ALARM_ACTIVE, false).apply();
        // Use super method
        WakefulIntentService.cancelAlarms(ctxt);
    }

    public static void scheduleAlarms(WakefulIntentService.AlarmListener listener, Context ctxt, boolean force) {
        // Set the ALARM_ACTIVE flag, otherwise the listener won't do it
        Log.i("TauTracker" + "QueryService", "ALARMS_ACTIVE_FLAG = true");
        SharedPreferences prefs = ctxt.getSharedPreferences("com.commonsware.cwac.wakeful.WakefulIntentService", 0);
        prefs.edit().putBoolean(QueryService.PREFS_WAKEFUL_ALARM_ACTIVE, true).apply();
        // Use super method
        WakefulIntentService.scheduleAlarms(listener, ctxt, force);
    }

    protected Notification buildNotification(String title, String text, int iconId,
                                             PendingIntent pendingNotificationIntent, boolean vibrate) {
        //TODO: Color icon according to grade?
        //TODO Group notifications together?
        //TODO: Add TAU logo to icon?
        //TODO: Do we need this for compatibility? Maybe switch to regular class
        android.support.v4.app.NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                        .setSmallIcon(iconId)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setLights(Color.RED, 3000, 3000)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setColor(ContextCompat.getColor(this, R.color.colorIcon))
                        .setAutoCancel(true);
        if (pendingNotificationIntent != null) {
            builder = builder.setContentIntent(pendingNotificationIntent);
        }
        if (vibrate) {
            builder = builder.setVibrate(VIBRATE_PATTERN);
        }
        return builder.build();
    }

    protected boolean isGradeInteresting(String grade) {
        if (grade.equals(NBSP_STRING)) {
            return false;
        }
        if (grade.equals("*")) {
            return false;
        }
        return true;
    }

    protected void showNotification(String title, String text, boolean vibrate) {
        // Gets an instance of the NotificationManager service
        Notification notif = buildNotification(title, text, R.drawable.ic_note_add, null, vibrate);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL, "Default notification channel", NotificationManager.IMPORTANCE_HIGH);
            mNotifyMgr.createNotificationChannel(mChannel);
        }
        // Builds the notification and issues it
        mNotifyMgr.notify(getAndIncrementNotificationId(this), notif);
    }

    private void queryGrades (TauImsScraper scraper, TauSettings settings) throws InternetConnectionException, WebParsingException {
        // Read grades from website
        HashMap<String, String> grades = scraper.get_course_grades(settings.mAcademicPeriod,
                settings.mMajorTckey);

        Log.i("TauTracker" + this.getClass().getSimpleName(), "Found " + grades.size() + " grades");
        // Read saved grades
        //TODO: Can do some more code reuse between grades and scans
        SharedPreferences gradesMap = getSharedPreferences(PREFS_GRADES_MAP_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = gradesMap.edit();

        for (String course : grades.keySet()) {
            String grade = grades.get(course);
            if (!gradesMap.getString(course, "").equals(grade)) {
                if (isGradeInteresting(grade)) {
                    // Grade changed to non-default value, notify user
                    Log.i("TauTracker" + this.getClass().getSimpleName(), "Grade changed for course " + course + ", notifying user");
                    showNotification(getString(R.string.new_grade_message), course + " - " + grade, true);
                }
                // Update the grade map
                editor.putString(course, grade);
            }
        }
        editor.apply();
    }

    private void queryScans(TauImsScraper scraper, TauSettings settings) throws InternetConnectionException, WebParsingException {
        HashMap<String, Boolean> scans = scraper.get_exam_scans(settings.mAcademicPeriod,
                settings.mMajorTckey);

        Log.i("TauTracker" + this.getClass().getSimpleName(), "Found " + scans.size() + " scans");
        // Read saved scans
        SharedPreferences scansMap = getSharedPreferences(PREFS_SCANS_MAP_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = scansMap.edit();

        for (String course : scans.keySet()) {
            boolean hasScan = scans.get(course);
            if (hasScan && (!scansMap.getBoolean(course, false))) {
                // New scan available
                Log.i("TauTracker" + this.getClass().getSimpleName(), "Scan status changed for course " + course + ", notifying user");
                String title = getString(R.string.new_scan_message);
                showNotification(title, course, true);
                editor.putBoolean(course, true);
            }
        }
        editor.apply();
    }

    private void stopQueryAndShowError(ScraperException e) {
        QueryService.cancelAlarms(this);
        // Build pending intent for notification's action
        // TODO: Call separate intent to show error message immediately but not focus activity
        Intent notificationIntent = new Intent(getApplicationContext(), TauLoginActivity.class);
        notificationIntent.putExtra(TauLoginActivity.INTENT_EXCEPTION_MESSAGE, e.getMessage());
        int notificationCounter = getAndIncrementNotificationId(this);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(getApplicationContext(),
                notificationCounter, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Show error notification, with on-click event
        Notification notif = buildNotification(getString(R.string.error_scraping),
                getString(R.string.text_stopped_task),
                R.drawable.ic_error,
                pendingNotificationIntent, false);

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it
        mNotifyMgr.notify(notificationCounter, notif);
    }

    public static void clearDB(Context ctx) {
        SharedPreferences scansMap = ctx.getSharedPreferences(PREFS_SCANS_MAP_FILE, Context.MODE_PRIVATE);
        scansMap.edit().clear().apply();
        SharedPreferences gradesMap = ctx.getSharedPreferences(PREFS_GRADES_MAP_FILE, Context.MODE_PRIVATE);
        gradesMap.edit().clear().apply();
    }

    @Override
    protected void doWakefulWork(Intent intent) {
        //TODO: Read the SharedPreferences only once
        TauSettings settings = TauSettings.loadSharedPrefs(this);
        TauImsScraper scraper = new TauImsScraper();
        //TODO: Test with internet disconnects, password changes, make sure we behave properly
        try {
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Doing wakeful work");
            scraper.login(settings.mIdNumber, settings.mUsername, settings.mPassword);
            queryGrades(scraper, settings);
            queryScans(scraper, settings);

        } catch (WebParsingException e) {
            // This exception type should not occur - if it did we want to stop querying
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Parsing error in doWakefulWork");
            e.printStackTrace();
            stopQueryAndShowError(e);
            return;
        } catch (LoginFailedException e) {
            // This exception type should not occur - if it did we want to stop querying
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Login error in doWakefulWork");
            e.printStackTrace();
            stopQueryAndShowError(e);
            return;
        }  catch (InternetConnectionException e) {
            // Notice that in this case we keep running, we assume the error is temporary
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Internet error in doWakefulWork");
            e.printStackTrace();
        }

        if (QueryService.isFirstRun(this)) {
            this.showNotification(getString(R.string.tracking_grades), getString(R.string.waiting_grades), false);
            QueryService.setFirstRun(this, false);
        }

    }

    public static void runImmediately(Context context) {
        Intent i = new Intent(context, AlarmReceiver.class);
        context.sendBroadcast(i);
    }

    public static boolean isAlarmScheduled(Context ctxt) {
        SharedPreferences prefs = ctxt.getSharedPreferences("com.commonsware.cwac.wakeful.WakefulIntentService", 0);
        return prefs.getBoolean(QueryService.PREFS_WAKEFUL_ALARM_ACTIVE, false);
    }

    /**
     * Since alarms can be rescheduled directly from QueryAlarmListener (on boot),
     * we have this function to allow QueryService to reinitialize whatever it needs.
     */
    public static void handleAlarmReschedule(Context ctxt) {
        // Notify the user on the next task run
        QueryService.setFirstRun(ctxt, true);
    }

    private static boolean isFirstRun(Context ctxt) {
        SharedPreferences prefs = ctxt.getSharedPreferences(PREFS_SERVICE_FILE, 0);
        return prefs.getBoolean(QueryService.PREFS_SERVICE_FIRST_RUN, true);
    }

    private static void setFirstRun(Context ctxt, boolean isFirstRun) {
        SharedPreferences prefs = ctxt.getSharedPreferences(PREFS_SERVICE_FILE, 0);
        prefs.edit().putBoolean(QueryService.PREFS_SERVICE_FIRST_RUN, isFirstRun).apply();
    }

    private static int getAndIncrementNotificationId(Context ctxt) {
        SharedPreferences prefs = ctxt.getSharedPreferences(PREFS_SERVICE_FILE, 0);
        int ans = prefs.getInt(QueryService.PREFS_SERVICE_NOTIF_ID, 137);
        prefs.edit().putInt(PREFS_SERVICE_NOTIF_ID, ans + 1).apply();
        Log.d("TauTracker" + "QueryService", "Notification id: " + ans);
        return ans;
    }

}
