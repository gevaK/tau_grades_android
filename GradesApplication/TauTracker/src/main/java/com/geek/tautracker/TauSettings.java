package com.geek.tautracker;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by GevaK on 29/10/2016.
 * This class is used to represent the settings relevant to TAU querying -
 * Id, User, Password, Year, Semester, Major
 */
public class TauSettings {

    final static String PREFS_LOGIN_FILE = "TAU_LOGIN_PREFS";
    final static String PREFS_LOGIN_ID = "TAU_ID";
    final static String PREFS_LOGIN_USER = "TAU_USER";
    final static String PREFS_LOGIN_PASSWORD = "TAU_PASS";
    final static String PREFS_MAJOR_NAMES_LIST = "TAU_MAJOR_NAMES_LIST";
    final static String PREFS_MAJOR_TCKEYS_LIST = "TAU_MAJOR_TCKEYS_LIST";
    final static String PREFS_QUERY_YEAR = "TAU_YEAR";
    final static String PREFS_QUERY_SEMESTER = "TAU_SEMESTER";
    final static String PREFS_QUERY_MAJOR = "TAU_MAJOR";

    final static String DEFAULT_YEAR = "2015";
    final static int DEFAULT_SEMESTER = Semester.SEM_A.ordinal();
    final static String DEFAULT_MAJOR_TCKEY = "-1";

    private static ArrayList<MajorC> majors = null;

    String mIdNumber;
    String mUsername;
    String mPassword;
    AcademicPeriod mAcademicPeriod;
    String mMajorTckey;

    public static TauSettings loadSharedPrefs(Context appContext) {
        SharedPreferences prefs = appContext.getSharedPreferences(PREFS_LOGIN_FILE,
                Context.MODE_PRIVATE);
        String idNumber = prefs.getString(PREFS_LOGIN_ID, "");
        String username = prefs.getString(PREFS_LOGIN_USER, "");
        String password = prefs.getString(PREFS_LOGIN_PASSWORD, "");
        AcademicPeriod ap = new AcademicPeriod(
                prefs.getString(PREFS_QUERY_YEAR, DEFAULT_YEAR),
                Semester.values()[prefs.getInt(PREFS_QUERY_SEMESTER, DEFAULT_SEMESTER)] );

        String majorTckey = prefs.getString(PREFS_QUERY_MAJOR, DEFAULT_MAJOR_TCKEY);

        return new TauSettings(idNumber, username, password, ap, majorTckey);
    }
    public TauSettings(String idNumber, String username, String password,
                       AcademicPeriod academicPeriod, String majorTckey) {
        mIdNumber = idNumber;
        mUsername = username;
        mPassword = password;
        mAcademicPeriod = academicPeriod;
        mMajorTckey = majorTckey;
    }

    /**
     * Save login data to shared preferences:
     * id, user and password (as plaintext ): )
     * Year, Semester, Major
     */
    public void saveSharedPrefs(Context appContext) {
        SharedPreferences.Editor editor =
                appContext.getSharedPreferences(PREFS_LOGIN_FILE, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_LOGIN_ID, mIdNumber);
        editor.putString(PREFS_LOGIN_USER, mUsername);
        editor.putString(PREFS_LOGIN_PASSWORD, mPassword);
        editor.putString(PREFS_QUERY_YEAR, mAcademicPeriod.mYear);
        editor.putInt(PREFS_QUERY_SEMESTER, mAcademicPeriod.mSem.ordinal());
        editor.putString(PREFS_QUERY_MAJOR, mMajorTckey);
        editor.apply();
    }

    public static ArrayList<MajorC> getSavedMajors(Context ctxt) {
        if (TauSettings.majors == null) {
            // Use this lazy utility class to save / load an ordered list
            TinyDB db = new TinyDB(ctxt);
            ArrayList<String> majorNames = db.getListString(PREFS_MAJOR_NAMES_LIST);
            ArrayList<String> majorTckeys = db.getListString(PREFS_MAJOR_TCKEYS_LIST);
            TauSettings.majors = new ArrayList<>(majorNames.size());
            for (int i = 0; i < majorNames.size(); i++) {
                MajorC mj = new MajorC(majorNames.get(i), majorTckeys.get(i));
                TauSettings.majors.add(mj);
            }
            if (TauSettings.majors.isEmpty()) {
                TauSettings.majors.add(new MajorC(ctxt.getString(R.string.default_major_name), DEFAULT_MAJOR_TCKEY));
            }
        }
        return TauSettings.majors;
    }

    public static void saveMajors(Context ctxt, ArrayList<MajorC> majorsList) {
        TauSettings.majors = majorsList;
        ArrayList<String> majorNames = new ArrayList<>(majorsList.size());
        ArrayList<String> majorTckeys = new ArrayList<>(majorsList.size());
        for (int i = 0; i < majorsList.size(); i++) {
            majorNames.add(majors.get(i).mName);
            majorTckeys.add(majors.get(i).mTckey);
        }
        // Use this lazy utility class to save / load an ordered list
        TinyDB db = new TinyDB(ctxt);
        db.putListString(PREFS_MAJOR_NAMES_LIST, majorNames);
        db.putListString(PREFS_MAJOR_TCKEYS_LIST, majorTckeys);
    }
}
