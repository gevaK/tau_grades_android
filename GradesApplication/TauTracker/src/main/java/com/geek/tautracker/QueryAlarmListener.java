/***
 Copyright (c) 2009-11 CommonsWare, LLC

 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License. You may obtain
 a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package com.geek.tautracker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class QueryAlarmListener implements WakefulIntentService.AlarmListener {
    static final long DELAY_MS = 15 * 1000;
    static final long INTERVAL_MS = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

    public void scheduleAlarms(AlarmManager mgr, PendingIntent pi,
                               Context ctxt) {
        // This will only actually schedule if the ALARM_ACTIVE flag was turned on
        // It is turned on when QueryService.scheduleAlarms is called (not WakefulIntentService!),
        // and turned off when QueryService.cancelAlarms is called
        if (QueryService.isAlarmScheduled(ctxt)) {
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Scheduling alarms");
            QueryService.handleAlarmReschedule(ctxt); // Notify service about reschedule
            mgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + DELAY_MS,
                    INTERVAL_MS, pi);
        } else {
            Log.i("TauTracker" + this.getClass().getSimpleName(), "Skipping alarms schedule because flag is off");
        }
    }

    public void sendWakefulWork(Context ctxt) {
        WakefulIntentService.sendWakefulWork(ctxt, QueryService.class);
    }

    public long getMaxAge(Context ctxt) {
        return(INTERVAL_MS * 2);
    }
}