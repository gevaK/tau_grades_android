package com.geek.tautracker;

/**
 * Created by GevaK on 28/10/2016.
 */

enum Semester {
    SEM_A("1"), SEM_B("2");

    String mId;
    Semester(String id) {
        this.mId = id;
    }

    public String getId() {
        return this.mId;
    }

};

class AcademicPeriod {

    String mYear;
    Semester mSem;

    public AcademicPeriod(String year, Semester sem) {
        this.mYear = year;
        this.mSem = sem;
    }

    public String toString() {
        return this.mYear + this.mSem.getId();
    }
}

enum Major {
    CS1("0102036811050366141"),
    MATH1("0101036611050368141"),
    CS2("0201036830200000201");

    String mTckey;
    Major(String tckey) {
        this.mTckey = tckey;
    }
}

class MajorC extends Object {
    String mTckey;
    String mName;
    public MajorC(String name, String tckey) {
        this.mName = name;
        this.mTckey = tckey;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MajorC)) {
            return false;
        }
        MajorC m = (MajorC) o;
        return this.mName.equals(m.mName) && this.mTckey.equals(m.mTckey);
    }

    public String toString() {
        return this.mName;
    }
}