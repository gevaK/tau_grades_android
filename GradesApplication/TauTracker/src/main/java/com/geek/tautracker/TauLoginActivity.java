package com.geek.tautracker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A login screen that offers login via email/password.
 */
public class TauLoginActivity extends AppCompatActivity {

    public static final String INTENT_EXCEPTION_MESSAGE = "ExceptionMessage";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mIdView;
    private EditText mUserView;
    private EditText mPasswordView;
    private EditText mYearView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView mErrorView;
    private Spinner mSemSpinner;
    private Spinner mMajorSpinner;
    private Button mLogInButton;
    private boolean mIsTaskActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tau_login);

        // Set up action bar.
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_grades_2);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        // Set up the login form.
        mIdView = (EditText) findViewById(R.id.id_number);
        mUserView = (EditText) findViewById(R.id.username);
        mYearView = (EditText) findViewById(R.id.year);
        mSemSpinner = (Spinner) findViewById(R.id.semester);
        mMajorSpinner = (Spinner) findViewById(R.id.major);
        mErrorView = (TextView) findViewById(R.id.general_error);
        mPasswordView = (EditText) findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    buttonPressed();
                    return true;
                }
                return false;
            }
        });

        mLogInButton = (Button) findViewById(R.id.log_in_button);
        assert mLogInButton != null;
        mLogInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonPressed();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // Handle new intent if necessary
        dynamicInit(getIntent());

    }

    private void refreshTaskStatus() {
        //TODO: This can get out of sync on errors, has to be called earlier
        if (QueryService.isAlarmScheduled(this)) {
            mLogInButton.setBackgroundResource(android.R.drawable.btn_default);
            mLogInButton.setText(R.string.button_action_cancel_task);
            mIdView.setEnabled(false);
            mUserView.setEnabled(false);
            mPasswordView.setEnabled(false);
            mYearView.setEnabled(false);
            mSemSpinner.setEnabled(false);
            mMajorSpinner.setEnabled(false);
            mIsTaskActive = true;
        } else {
            mLogInButton.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            mLogInButton.setText(R.string.action_sign_in);
            mIdView.setEnabled(true);
            mUserView.setEnabled(true);
            mPasswordView.setEnabled(true);
            mYearView.setEnabled(true);
            mSemSpinner.setEnabled(true);
            mMajorSpinner.setEnabled(true);
            mIsTaskActive = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_show_grades:
                Intent intent = new Intent(this, GradesListActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_clear_db:
                QueryService.clearDB(this);
                Toast.makeText(this, R.string.toast_cleared_db, Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_cancel_task:
                handleCancelAlarmsPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleCancelAlarmsPressed() {
        QueryService.cancelAlarms(this);
        // Reset button color
        refreshTaskStatus();
        Toast.makeText(this, R.string.toast_task_cancelled, Toast.LENGTH_LONG).show();
    }

    /**
     * Takes care of all 'smart', state related initialization
     */
    public void dynamicInit(Intent intent) {
        Log.i("TauTracker" + this.getClass().getSimpleName(), "inside dynamicInit");
        // Update the task status
        refreshTaskStatus();
        // Autofill fields.
        AutoFillInputs();
        Bundle extras = intent.getExtras();
        if (extras != null) { // Show error message
            if (extras.containsKey(INTENT_EXCEPTION_MESSAGE)) {
                // extract the extra-data in the notification
                String errMsg = extras.getString(INTENT_EXCEPTION_MESSAGE);
                showErrorAlert(getString(R.string.error_scrape_failed) + "\n\nException:\n" +
                        errMsg);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshTaskStatus();
    }

    @Override
    public void onNewIntent(Intent intent) {
        dynamicInit(intent);
    }

    /**
     * Fill inputs by saved preferences
     */
    private void AutoFillInputs() {
        TauSettings settings = TauSettings.loadSharedPrefs(this);
        mIdView.setText(settings.mIdNumber);
        mUserView.setText(settings.mUsername);
        mYearView.setText(settings.mAcademicPeriod.mYear);
        mSemSpinner.setSelection(settings.mAcademicPeriod.mSem.ordinal());
        refreshMajorsSpinner(settings.mMajorTckey);

        // Change focus to password if other fields were autofilled
        if (!settings.mIdNumber.isEmpty() && !settings.mUsername.isEmpty()) {
            mPasswordView.requestFocus();
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void buttonPressed() {
        Log.d("TauTracker" + this.getClass().getSimpleName(), "inside buttonPressed");
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mIdView.setError(null);
        mUserView.setError(null);
        mPasswordView.setError(null);
        mErrorView.setText("");
        mErrorView.setError(null);

        if (mIsTaskActive) {
            handleCancelAlarmsPressed();
            return;
        }

        TauSettings settings = buildTauSettings();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(settings.mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(settings.mPassword)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(settings.mUsername)) {
            mUserView.setError(getString(R.string.error_field_required));
            focusView = mUserView;
            cancel = true;
        } else if (!isUserValid(settings.mUsername)) {
            mUserView.setError(getString(R.string.error_invalid_username));
            focusView = mUserView;
            cancel = true;
        }

        // Check for a valid id.
        if (TextUtils.isEmpty(settings.mIdNumber)) {
            mIdView.setError(getString(R.string.error_field_required));
            focusView = mIdView;
            cancel = true;
        } else if (!isIdValid(settings.mIdNumber)) {
            mIdView.setError(getString(R.string.error_invalid_id));
            focusView = mIdView;
            cancel = true;
        }

        // Check for a valid year.
        if (TextUtils.isEmpty(settings.mAcademicPeriod.mYear)) {
            mYearView.setError(getString(R.string.error_field_required));
            focusView = mYearView;
            cancel = true;
        } else if (!isYearValid(settings.mAcademicPeriod.mYear)) {
            mYearView.setError(getString(R.string.error_invalid_year));
            focusView = mYearView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(settings);
            mAuthTask.execute((Void) null);
        }
    }

    private TauSettings buildTauSettings() {
        // Store values at the time of the login attempt.
        String idNumber = mIdView.getText().toString();
        String username = mUserView.getText().toString();
        String password = mPasswordView.getText().toString();
        String year = mYearView.getText().toString();
        int semesterOrdinal = mSemSpinner.getSelectedItemPosition();
        MajorC major = (MajorC) mMajorSpinner.getSelectedItem();
        AcademicPeriod ap = new AcademicPeriod(year,
                Semester.values()[semesterOrdinal]);

        return new TauSettings(idNumber, username, password,
                ap, major.mTckey);
    }

    private boolean isIdValid(String idNumber) {
        //TODO: Add ID validation?
        return true;
    }

    private boolean isYearValid(String year) {
        int iYear = Integer.parseInt(year);
        return  iYear > 2000 && iYear < 3000;
    }

    private boolean isUserValid(String username) { return true; }

    private boolean isPasswordValid(String password) {
        return true;
    }

    /**
     * Refills the majors spinner.
     * @param prevTckey The tckey of the previously selected major, or null if you don't care about autofill.
     */
    private void refreshMajorsSpinner(String prevTckey) {
        ArrayList<MajorC> majors = TauSettings.getSavedMajors(this);
        mMajorSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, majors));
        mMajorSpinner.setSelection(0);
        if (prevTckey != null) {
            for (int i = 0; i < majors.size(); i++) {
                if (prevTckey.equals(majors.get(i).mTckey)) {
                    mMajorSpinner.setSelection(i);
                }
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // Hide keyboard if necessary
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void showErrorAlert(String exceptionMessage) {
        new AlertDialog.Builder(TauLoginActivity.this)
                .setTitle(getString(R.string.error))
                .setMessage(exceptionMessage)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .show();
    }

    private class LoginTaskResult {
        boolean success;
        ScraperException exception;
        ArrayList<MajorC> majorList;
        public LoginTaskResult(boolean success, ScraperException exception, ArrayList<MajorC> majorList) {
            this.success = success;
            this.exception = exception;
            this.majorList = majorList;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, LoginTaskResult> {

        private final TauSettings mSettings;
        ScraperException mException;

        UserLoginTask(TauSettings settings) {
            mSettings = settings;
        }

        @Override
        protected LoginTaskResult doInBackground(Void... params) {
            // Attempt authentication against TAU IMS
            // Only if we succeed we create the background task
            TauImsScraper scraper = new TauImsScraper();
            boolean success = false;
            ArrayList<MajorC> majorList;
            try {
                scraper.login(mSettings.mIdNumber, mSettings.mUsername, mSettings.mPassword);
                majorList = scraper.get_majors();

            } catch (LoginFailedException e) {
                // e.printStackTrace();
                // In this case we do not show any error to the user other than 'password incorrect'
                return new LoginTaskResult(false, e, null);
            } catch (InternetConnectionException e) {
                e.printStackTrace();
                return new LoginTaskResult(false, e, null);
            } catch (WebParsingException e) {
                e.printStackTrace();
                return new LoginTaskResult(false, e, null);
            }
            // No exception means login success
            return new LoginTaskResult(true, null, majorList);
        }

        protected void updateMajorList() {
            refreshMajorsSpinner(mSettings.mMajorTckey);
            mErrorView.setText(getString(R.string.error_major_list_updated));
            mErrorView.setError("");
            mErrorView.requestFocus();
            /*Toast.makeText(getApplicationContext(), R.string.error_major_list_updated,
                    Toast.LENGTH_LONG).show();*/
        }

        @Override
        protected void onPostExecute(final LoginTaskResult result) {
            mAuthTask = null;
            showProgress(false);
            if (result.success) {
                ArrayList<MajorC> oldMajors = TauSettings.getSavedMajors(getApplicationContext());
                ArrayList<MajorC> newMajors = result.majorList;
                if (!newMajors.equals(oldMajors)) {
                    // Check for update in majors
                    Log.i("TauTracker" + this.getClass().getSimpleName(), "Major list updated");
                    Log.i("TauTracker" + this.getClass().getSimpleName(), "Old Majors: " + oldMajors.toString());
                    Log.i("TauTracker" + this.getClass().getSimpleName(), "New Majors: " + newMajors.toString());
                    TauSettings.saveMajors(getApplicationContext(), newMajors);
                    updateMajorList();
                }
                else { // We only start if the majors list wasn't updated
                    // Save login data to shared preferences
                    //TODO: Maybe don't save password in plaintext, pass it with the intent - but this ruins boot autorun
                    mSettings.saveSharedPrefs(getApplicationContext());

                    // Start wakeful intent service alarms
                    // We also manually run the query service immediately, to detect new grades
                    QueryService.scheduleAlarms(new QueryAlarmListener(),
                            getApplicationContext(), false);

                    QueryService.runImmediately(getApplicationContext());

                    //TODO: Add ability to easily tell if alarms are on? Maybe status bar icon?
                    Toast.makeText(getApplicationContext(), R.string.toast_task_active,
                            Toast.LENGTH_LONG).show();
                }

            } else if (result.exception instanceof LoginFailedException) {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            } else {
                showErrorAlert(getString(R.string.error_no_tau_access) +
                        "\n\nException:\n" +
                        result.exception.getMessage() );
            }

            // Reset button color
            refreshTaskStatus();
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

