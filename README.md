# **TauTracker** #
## What is it? ##
An android application for tracking TAU grades during the exam period.  
It logs in to ims.tau.ac.il every 15 minutes, and notifies the user if there are any new grades or exam scans.

*** Warning ***:  
Using the app is similar to logging into the website every 15 minutes and checking your grades.  
There is no reason why this would make the university angry, but I **DO NOT** take responsibility for it.

## Installation instructions ##
1. Since the app isn't on the Google Play Store, to install it you first have to go into Settings->Security and check the 'Unknown Sources' option,
which allows the installation of apps from 'unknown' sources.
2. After changing the above setting, download the .apk file of the latest version from this repositorys 'Downloads' section to your phone and open it to install.

## Usage information ##
1. TauTracker checks the TAU website every 15 minutes. As far as my own tests show, this does not affect battery usage noticeably.
2. On the first (ever) login the app will obtain your list of majors from the TAU website, and you will have to login again to start tracking.
3. Once you're logged in and the app is tracking your grades, it will continue to do so automatically even after you reboot your phone,
until you order it to stop (via the 'Cancel' button).
4. The most common error is selecting the wrong semester by mistake. Notice that selecting 2016 as the year refers to the academic year 2016-2017.

## Security ##
1. TauTracker saves your password to the TAU website on your device, unencrypted, within a file that should only be accesible to this app.  
If this bothers you at all, contact me and I might implement the alternative of only saving it in device memory.

## Developer information ##
1. The code is not very well-designed due to the addition of unplanned features, and I currently don't have the time to refactor it.
2. You can contact me at gevakip@gmail.com

# **Changelog** #
###v1.1###
1. Added dynamic querying of majors from TAU website to support all possible majors.
2. Fixed bug when the user had no exam scans on given semester.
3. Cancelled vibration on uninteresting notifications.

# **TODO** #
1. Change major selection to checklist and allow querying multiple majors.
2. Fix bug with errors not updating UI correctly when the app is focused.